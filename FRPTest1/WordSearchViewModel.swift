//
//  WordSearchViewModel.swift
//  FRPTest1
//
//  Created by Sumedh Ravi on 28/06/18.
//  Copyright © 2018 Sumedh Ravi. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class WordSearchViewModel {
    
    struct Word: Codable {
        var word: String
    }
    
    var searchText = BehaviorSubject<String>(value: "")
    var dataSourceStream = BehaviorSubject<[String]>(value: [""])
    var disposeBag = DisposeBag()
    
    init() {
        searchText.asObservable().map { [unowned self] (searchText) -> URL? in
            return self.getUrl(for: searchText)
        }
            .flatMapLatest { (url) -> Observable<[String]> in
                return self.fetchApiResults(forUrl: url)
        }
            .bind(to: dataSourceStream)
            .disposed(by: disposeBag)
    }
    
    func getMeanings(queryString: String) -> Observable<[String]> {
        guard let url = getUrl(for: queryString) else {
            return Observable.just([])
        }
        
        return fetchApiResults(forUrl: url)
    }
    
    func getUrl(for query: String, maxResults: Int = 10) -> URL? {
        guard !query.isEmpty else { return nil }
        let urlString = "https://api.datamuse.com/words?rel_syn=\(query)&max=\(maxResults)"
        guard let url = URL(string: urlString) else {
            print("Invalid URL")
            return nil
        }
        return url
    }
    
    func fetchApiResults(forUrl url: URL?) -> Observable<[String]> {
        guard let url = url else { return Observable.just([]) }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        let sessionDataStream = Observable<Data>.create { observer in
            let task = session.dataTask(with: request) { (data, response, error) in
                guard let response = response, let data = data else {
                    observer.onError(error ?? RxCocoaURLError.unknown)
                    return
                }

                guard response is HTTPURLResponse else {
                    observer.onError(RxCocoaURLError.nonHTTPResponse(response: response))
                    return
                }

                observer.onNext(data)
                observer.onCompleted()
            }

            task.resume()

            return Disposables.create {
                task.cancel()
            }
        }
        return sessionDataStream.map { (data) -> [String] in
            return self.parseWords(data: data)
        }
//        return session.rx.data(request: request).asObservable().map({ (data) -> [String] in
//            return self.parseWords(data: data)
//        })
    }
    
    func parseWords(data: Data) -> [String] {
        do {
            let decoder = JSONDecoder()
            let words = try decoder.decode([Word].self, from: data)
            
            let synonyms = words.map { (word) in
                return word.word
            }
            return synonyms
        }
        catch let error {
            print(error)
        }
        return []
    }
    
}
