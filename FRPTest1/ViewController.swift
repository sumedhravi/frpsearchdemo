//
//  ViewController.swift
//  FRPTest1
//
//  Created by Sumedh Ravi on 26/06/18.
//  Copyright © 2018 Sumedh Ravi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var wordSearchBar: UISearchBar!
    @IBOutlet weak var wordsTableView: UITableView!

    private let cellIdentifier = "WordTableViewCell"
    private let wordsViewModel = WordSearchViewModel()
    let disposeBag = DisposeBag()
    private var alertVC: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRx()
        subscribeToKeyboardNotifications()
    }
    
    private func setupRx() {
        wordSearchBar.rx.text
            .orEmpty
            .debounce(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .bind(to: wordsViewModel.searchText)
            .disposed(by: disposeBag)
        
        wordsViewModel.dataSourceStream.asObservable().bind(to: wordsTableView.rx.items(cellIdentifier: cellIdentifier, cellType: UITableViewCell.self)) {
            row, element, cell in
            cell.textLabel?.text = "\(row + 1).  \(element)"
            }
            .disposed(by: disposeBag)
        
        Observable.zip(wordsViewModel.dataSourceStream, wordsViewModel.searchText)
            .subscribe(onNext: { [unowned self] (dataSource, searchString) in
                if (dataSource.isEmpty && !searchString.isEmpty) {
                    self.showEmptyAlert()
                }
            })
            .disposed(by: disposeBag)
        
        
        
    }
    
    private func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardRectDidChange), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardRectDidChange), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    @objc func keyboardRectDidChange(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            wordsTableView.contentInset = UIEdgeInsets.zero
        } else {
            wordsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
    }
    
    private func showEmptyAlert() {
        DispatchQueue.main.async {
        self.alertVC?.dismiss(animated: false)
        self.alertVC = UIAlertController(title: "Alert", message: "No words Found!", preferredStyle: UIAlertControllerStyle.alert)
        self.alertVC?.addAction(UIAlertAction(title: "OK", style: .default))
        
            self.present(self.alertVC!, animated: true)
        }
    }
}
